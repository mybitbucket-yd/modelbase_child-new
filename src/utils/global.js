import Router from '../router/index'
import CryptoJS from 'crypto-js'
/* 加密工具类 */
export const AES = {
  // 加密
  encode(data, defaultKey = 'abcdsxyzhkj12345') {
    let srcs = CryptoJS.enc.Utf8.parse(JSON.stringify(data))
    let key = CryptoJS.enc.Utf8.parse(defaultKey)
    let encrypted = CryptoJS.AES.encrypt(srcs, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    return encrypted.toString()
  },
  //   解密
  decode(data, defaultKey = 'abcdsxyzhkj12345') {
    let key = CryptoJS.enc.Utf8.parse(defaultKey)
    let decrypt = CryptoJS.AES.decrypt(data, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    let dataStr = CryptoJS.enc.Utf8.stringify(decrypt).toString()
    return JSON.parse(dataStr)
  }
}
// MD5加密
// export const MD5 = {
//   encode(data) {
//     return CryptoJS.MD5(data).toString() /* toString后会变成Hex 32位字符串*/
//   }
// }
// // base64加密
// export const Base64 = {
//   // 加密
//   encode(str) {
//     return btoa(
//       encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
//         return String.fromCharCode('0x' + p1)
//       })
//     )
//   },
//   // 解密
//   decode(str) {
//     // Going backwards: from bytestream, to percent-encoding, to original string.
//     return decodeURIComponent(
//       atob(str)
//         .split('')
//         .map(function (c) {
//           return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
//         })
//         .join('')
//     )
//   }
// }
// 通过路由名称获取路由路径
const getPathByName = function (names) {
  let path = null
  const modelBaseRoutes = storage.get('MODELBASEROUTES')
  try {
    modelBaseRoutes.forEach(item => {
      let arr = []
      for (const key in names) {
        arr.push(item[key] && item[key] == names[key])
      }
      let bol = arr.every(item => {
        return item == true
      })
      if (bol) {
        path = item.path
        throw new Error('end')
      }
    })
  } catch (e) {
    if (e.message != 'end') throw e
  }
  return path
}
// 统一路由跳转
const pushHandler = function (np, params = null) {
  // np=>路由名称  params=>路由参数
  // 1.  np:{
  //         name:"",
  //         path:"",
  //         ...
  //     }
  // 2. np:String
  console.log(np, params, '******路由信息******')
  // 参数加密
  let query = params ? { ps: AES.encode(params) } : {}
  if (typeof np == 'string') {
    let path = null
    // 子级-父级- 祖级-......   例：统计-任务管理
    if (np.includes('-')) {
      let names = {}
      let arr = np.split('-')
      let str = ''
      arr.forEach(mn => {
        names[str + 'name'] = mn
        str += 'p'
      })
      path = getPathByName(names)
    } else {
      path = getPathByName({ name: np })
    }
    if (path) {
      Router.push({ path: path, query })
    } else {
      console.log('路由不存在')
    }
  }
  if (typeof np == 'object') {
    if (np.path) {
      Router.push({ path: np.path, query })
    } else {
      console.log('缺少路由信息')
    }
  }
}
// 存储
export const storage = {
  // 设置存储
  set: function (key, value, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少key')
      return
    }
    let aesKey = AES.encode('MODEL_' + key)
    let aesValue = AES.encode(value)
    window[type + 'Storage'].setItem(aesKey, aesValue)
  },
  //   获取存储
  get: function (key, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少key')
      return
    }
    let aesKey = AES.encode('MODEL_' + key)
    let str = window[type + 'Storage'].getItem(aesKey)
    if (str) {
      return AES.decode(str)
    } else {
      return null
    }
  },
  //   删除存储
  clear: function (key, type = 'session') {
    if (key == '' || !key) {
      console.log('缺少必要参数')
      return
    }
    let aesKey = AES.encode('MODEL_' + key)
    window[type + 'Storage'].removeItem(aesKey)
  },
  //   清空存储
  clearAll: function () {
    let sessKeys = Object.keys(sessionStorage)
    let localKeys = Object.keys(localStorage)
    for (const key in sessKeys) {
      if (AES.decode(key).includes('MODEL_')) {
        sessionStorage.removeItem(key)
      }
    }
    for (const key in localKeys) {
      if (AES.decode(key).includes('MODEL_')) {
        localStorage.removeItem(key)
      }
    }
  }
}
// echarts图表字体自适应
const selfAdaption = function (res) {
  let clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
  if (!clientWidth) return
  let fontSize = clientWidth / 1920
  return res * fontSize
}
export const ModelGF = {
  install(vue, options) {
    vue.prototype.$ModelGF = {
      getPathByName,
      pushHandler,
      //   Base64,
      AES,
      //   MD5,
      storage,
      selfAdaption
    }
  }
}
