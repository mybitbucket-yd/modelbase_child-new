window.addEventListener('storage', function (e) {
  console.log(e, 'storageChange')
  // 禁止手动浏览器修改sessionStorage的值
  if (sessionStorage.getItem(e.key)) {
    sessionStorage.setItem(e.key, e.oldValue)
  }
})
