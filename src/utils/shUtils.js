// import * as elementResizeDetector from 'element-resize-detector'
import { getGroups } from '../http/api'

export function getGroupsHandler() {
  //   1 所属地区
  //   2  变电站电压等级
  //   3 所属站点
  //   4 设备间隔
  //   5 设备电压等级
  //   6
  //   7 设备分类
  //   8 模型用途
  const types = [
    { key: 'areaList', id: '1' },
    { key: 'voltageList', id: '2' },
    { key: 'site_nameList', id: '3' },
    { key: 'device_spaceList', id: '4' },
    { key: 'device_voltageList', id: '5' },
    { key: 'device_typeList', id: '7' },
    { key: 'fileTypeList', id: '8' }
  ]
  return new Promise(function (resolve, reject) {
    const params = {}
    getGroups().then(res => {
      types.forEach(item => {
        // if (res && res.list[item.id]) {
        params[item.key] = res && res.list[item.id] ? res.list[item.id] : []
        // }
      })
      resolve(params)
    })
  })
}

// export function resize(el, id) {
//   let erd = elementResizeDetector()
//   erd.listenTo(document.getElementById(id), function (e) {
//     el.resize()
//   })
// }

export const statusHandler = {
  status: [
    { value: 1, label: '审核中', key: 'SH' },
    { value: 2, label: '未通过审核', key: 'SH' }, // 审核未通过
    { value: 3, label: '校正中', key: 'JZ' },
    { value: 4, label: '未通过校正', key: 'JZ' }, // 校正未通过
    { value: 5, label: '待发布', key: 'FB' },
    { value: 6, label: '已发布', key: 'FB' }
  ],
  getNameById: function (id) {
    let str = null
    this.status.forEach(item => {
      if (item.value == id) {
        str = item.label
      }
    })
    return str || '-'
  },
  getStatusByKey: function (key) {
    return this.status.filter(item => item.key == key)
  }
}
