import http from './http'
const { GET, POSTJSON, POSTFROM, DELETE, PUT } = { ...http }
// *******************************分组接口*******************************
const prefix = '/api/v1'
const prefixV2 = '/api/v2'
// 分组列表
export const getGroups = params => GET(prefix + '/groups', params)
// 创建分组
export const createGroups = params => POSTFROM(prefix + '/group', params)
// 修改分组
export const updateGroups = params => POSTJSON(prefix + '/group/update', params)
// 删除分组
export const deleteGroups = params => GET(prefix + '/group/del', params)

// *******************************模型接口*******************************
// 全站模型列表

export const allPatters = params => GET(prefix + '/pattern/get_all_patters', params)

// 首页模型列表
export const getIndexModelList = params => GET(prefix + '/pattern/indexlist', params)
// 创建模型
export const createModel = params => POSTFROM(prefix + '/pattern/creat', params)
// 模型列表
export const getModelList = params => GET(prefix + '/pattern/list', params)
// 修改模型
export const updateModel = params => POSTFROM(prefix + '/pattern/update', params)
// 删除模型
export const deleteModel = params => GET(prefix + '/pattern/del', params)
// 模型数据
export const modeldata = params => GET(prefix + '/getmodeldata', params)
// 模型转换
export const modelConvert = params => GET(prefix + '/convert', params)
// 模型详情
export const modeldetails = params => GET(prefix + '/pattern/getone', params)
// 首页模糊搜索
export const searchList = params => GET('api/v1/getsearchList', params)
// 检查是否上传
export const checkUpload = params => POSTFROM(prefix + '/pattern/check_upload', params)

// 模型管理大接口
export const modelAdminList = params => GET(prefix + '/pattern/admin_list', params)

// 审核标准
export const auditStandard = params => GET(prefix + '/audit/getList', params)
// 审核详情
export const auditStanDetail = params => GET(prefix + '/pattern/getone', params)
// 完整性校验 审核
export const integrityCheck = params => GET(prefix + '/pattern/pattern_audit?' + params)

// 模型比对
export const modelCompar = params => GET(prefix + '/pattern/pattern_comparison', params)

// 模型审核结果
export const auditStatus = params => GET(prefix + '/pattern/audit_status', params)

export const auditStatusPost = params => POSTFROM(prefix + '/pattern/audit_status', params)

// 价格发布
export const priceRelea = params => GET(prefix + '/pattern/pricing_release?' + params)

// 区域分布
export const patternArea = params => GET(prefix + '/pattern/pattern_area', params)

//  版本历史
export const uploadVerison = params => GET(prefix + '/patten/get_upload_verison_list', params)

// 比对 同步
export const syncPattern = params => GET(prefix + '/pattern/sync_pattern?' + params)

// *******************************系统设置*******************************

export const auditAdd = params => POSTFROM(prefix + '/audit/add', params)

// *******************************收藏*******************************

// 添加收藏
export const addColletc = params => POSTFROM(prefix + '/colletc/add', params)
// 收藏列表
export const getColletcList = params => GET(prefix + '/colletc/getList', params)
// 移除收藏
export const delColletc = id => GET(prefix + '/colletc/del?id=' + id)

// 模型首页
export const modelIndex = id => GET(prefix + '/pattern/model_index')

// *******************************我的模型*******************************

// 我的上传
export const myUploadlist = params => GET(prefix + '/pattern/admin_list', params)

export const getmodeldata = params => GET(prefix + '/getmodeldata', params)
export const geTemConver = params => GET(prefix + '/audit/template_conversion', params)

// ***********************************2022.4.25***********************************
// 设备模型状态 数量
export const deviceCountStatus = () => GET(prefixV2 + '/device_pattern/count_published')

// 电压等级分类统计
export const deviceCountVoltage = () => GET(prefixV2 + '/device_pattern/count_voltage')
// 任务管理 - 审核校正列表
export const getSHJZlist = params => GET(prefixV2 + '/device_pattern/list', params)
// 发布待发布列表
export const getFBlist = params => GET(prefixV2 + '/device_version/getlist', params)
// 全站列表
export const getQZlist = params => GET(prefixV2 + '/scene/get_list', params)
// 设备模型创建
export const createEquipModel = params => POSTFROM(prefixV2 + '/device_pattern/create', params)
// 设备模型详细信息
export const getEquipInfo = id => GET(prefixV2 + '/device_pattern/getinfo?id=' + id)
// 审核标准
export const getAuditStand = () => GET(prefix + '/audit/getList')
// 全站模型审核基础信息
export const getAllAuditInfo = id => GET(prefixV2 + '/scene/scene_audit_report?id=' + id)
// 模型修改
export const updateDeviceCreate = params => POSTFROM(prefixV2 + '/device_pattern/update', params)
// 全站模型创建
export const createAllModel = params => POSTJSON(prefixV2 + '/scene/create', params)
// 定价发布
export const deVerUpdate = params => POSTJSON(prefixV2 + '/device_version/update', params)
// 全站信息修改
export const allUpdate = params => POSTJSON(prefixV2 + '/scene/update', params)
