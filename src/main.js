import './public-path'
import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store/index'
import uploader from 'vue-simple-uploader'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import md5 from 'js-md5'
import { ModelGF } from './utils/global'
import CodeEditor from 'bin-code-editor'

import './utils/storage'
import './utils/flexible'
// 全局组件
const components = require.context('./components/global', true, /\.vue$/)
components.keys().forEach(item => {
  let example = components(item)
  const reqComName = example.name || example.default.name || example.replace(/\.\/(.*)\.vue/, '$1')
  Vue.component(reqComName, example.default || example)
})

Vue.prototype.$md5 = md5

Vue.config.productionTip = false
let instance = null

function render(props = {}) {
  const { container } = props
  Vue.use(uploader)
  Vue.use(Element)
  Vue.use(CodeEditor)
  Vue.use(ModelGF)
  instance = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount(container ? container.querySelector('#ModelBase') : '#ModelBase')
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

export async function bootstrap() {
  console.log('[vue] vue app bootstraped')
}
export async function mount(props) {
  console.log('子应用挂载', props)
  render(props)
}
export async function unmount() {
  instance.$destroy()
  instance.$el.innerHTML = ''
  instance = null
}

export async function update() {
  instance.$destroy()
  instance.$el.innerHTML = ''
  instance = null
}
