import { storage } from '@/utils/global'
import MiddelPage from '@/layout/middelPage.vue'

const routes = [
  {
    path: '/',
    name: 'distvue',
    redirect: '/resourceManage/statistics'
  },
  {
    path: '/resourceManage',
    name: 'resourceManage',
    component: MiddelPage,
    redirect: '/resourceManage/statistics',
    meta: {
      name: '资源管理'
    },
    children: [
      {
        path: '/resourceManage/statistics',
        name: 'remstatistics',
        component: () => import('@/views/resourceManage/statistics.vue'),
        meta: {
          name: '统计',
          pname: '资源管理'
        }
      },
      {
        path: '/resourceManage/totalStationModel',
        name: 'totalStationModel',
        component: () => import('@/views/resourceManage/totalStationModel.vue'),
        meta: {
          name: '全站模型',
          pname: '资源管理'
        }
      },
      {
        path: '/resourceManage/equipmentModel',
        name: 'equipmentModel',
        component: () => import('@/views/resourceManage/equipmentModel.vue'),
        meta: {
          name: '设备模型',
          pname: '资源管理'
        }
      }
      //   {
      //     path: '/resourceManage/updateModelInfo',
      //     name: 'updateModelInfo',
      //     component: () => import('@/views/resourceManage/updateModelInfo.vue'),
      //     meta: {
      //       name: '模型修改',
      //       pname: '资源管理',
      //       noRenderRoute: true
      //     }
      //   }
      //   {
      //     path: '/resourceManage/regdistribution',
      //     name: 'regdistribution',
      //     component: () => import('@/views/resourceManage/regdistribution.vue'),
      //     meta: {
      //       name: '区域分布',
      //       pname: '资源管理',
      //       noRenderRoute: true
      //     }
      //   },
      //   {
      //     path: '/resourceManage/modelDetails',
      //     name: 'modelDetails',
      //     component: () => import('@/views/resourceManage/modelDetails.vue'),
      //     meta: {
      //       name: '模型详情',
      //       pname: '资源管理',
      //       noRenderRoute: true
      //     }
      //   }
    ]
  },

  {
    path: '/taskManage',
    name: 'taskManage',
    component: MiddelPage,
    redirect: '/taskManage/statistics',
    meta: {
      name: '任务管理'
    },
    children: [
      {
        path: '/taskManage/statistics',
        name: 'statistics',
        component: () => import('@/views/taskManage/statistics.vue'),
        meta: {
          name: '统计',
          pname: '任务管理'
        }
      },
      //   {
      //     path: '/taskManage/upload',
      //     name: 'upload',
      //     component: () => import('@/views/taskManage/upload.vue'),
      //     meta: {
      //       name: '上传',
      //       pname: '任务管理'
      //     }
      //   },
      {
        path: '/taskManage/toExamine',
        name: 'toExamine',
        component: () => import('@/views/taskManage/toExamine.vue'),
        meta: {
          name: '审核',
          pname: '任务管理'
        }
      },
      {
        path: '/taskManage/correcting',
        name: 'correcting',
        component: () => import('@/views/taskManage/correcting.vue'),
        meta: {
          name: '校正',
          pname: '任务管理'
        }
      },
      {
        path: '/taskManage/release',
        name: 'release',
        component: () => import('@/views/taskManage/release.vue'),
        meta: {
          name: '发布',
          pname: '任务管理'
        }
      }
    ]
  },

  {
    path: '/releaseManage',
    name: 'releaseManage',
    component: MiddelPage,
    redirect: '/releaseManage/waitingRelease',
    meta: {
      name: '发布管理'
    },
    children: [
      {
        path: '/releaseManage/waitingRelease',
        name: 'waitingRelease',
        component: () => import('@/views/releaseManage/waitingRelease.vue'),
        meta: {
          name: '待发布',
          pname: '发布管理'
        }
      },
      {
        path: '/releaseManage/released',
        name: 'released',
        component: () => import('@/views/releaseManage/released.vue'),
        meta: {
          name: '已发布',
          pname: '发布管理'
        }
      }
      //   {
      //     path: '/releaseManage/priceRelease',
      //     name: 'priceRelease',
      //     component: () => import('@/views/releaseManage/priceRelease'),
      //     meta: {
      //       name: '定价发布',
      //       pname: '发布管理',
      //       noRenderRoute: true
      //     }
      //   }
    ]
  },
  {
    path: '/financialManage',
    name: 'financialManage',
    component: () => import('@/views/financialManage/financialManage'),
    meta: {
      name: '财务管理'
    }
  },
  {
    path: '/roleManage',
    name: 'roleManage',
    component: () => import('@/views/roleManage/roleManage'),
    meta: {
      name: '角色管理'
    }
  },
  {
    path: '/orderManage/orderManage',
    name: 'orderManage',
    component: () => import('@/views/orderManage/orderManage.vue'),
    meta: {
      name: '订单管理'
    }
  },
  {
    path: '/systemSetting',
    name: 'systemSetting',
    component: MiddelPage,
    redirect: '/systemSetting/auditStandard',
    meta: {
      name: '设置'
    },
    children: [
      {
        path: '/systemSetting/auditStandard',
        name: 'auditStandard',
        component: () => import('@/views/systemSetting/auditStandard.vue'),
        meta: {
          name: '审核标准',
          pname: '设置'
        }
      },
      {
        path: '/systemSetting/modelClassification',
        name: 'modelClassification',
        component: () => import('@/views/systemSetting/modelClassification.vue'),
        meta: {
          name: '模型分类',
          pname: '设置'
        }
      },
      {
        path: '/systemSetting/vendorManagement',
        name: 'vendorManagement',
        component: () => import('@/views/systemSetting/vendorManagement.vue'),
        meta: {
          name: '厂商管理',
          pname: '设置'
        }
      },
      {
        path: '/systemSetting/saveSetting',
        name: 'saveSetting',
        component: () => import('@/views/systemSetting/saveSetting.vue'),
        meta: {
          name: '存储设置',
          pname: '设置'
        }
      },
      {
        path: '/systemSetting/syncSetting',
        name: 'syncSetting',
        component: () => import('@/views/systemSetting/syncSetting.vue'),
        meta: {
          name: '同步设置',
          pname: '设置'
        }
      },
      {
        path: '/systemSetting/dictionary',
        name: 'dictionary',
        component: () => import('@/views/systemSetting/dictionary.vue'),
        meta: {
          name: '字典设置',
          pname: '设置'
        }
      }
    ]
  },
  // 不渲染到路由导航中
  {
    path: '/uploadModel',
    name: 'uploadModel',
    component: () => import('@/views/uploadModel/uploadModel'),
    meta: {
      name: '上传模型',
      noRenderRoute: true
    }
  },
  {
    path: '/modelPreview',
    name: 'modelPreview',
    component: () => import('@/views/modelPreview/modelPreview'),
    meta: {
      name: '模型预览',
      noRenderRoute: true
    }
  },

  {
    path: '/collectManage',
    name: 'collectManage',
    component: () => import('@/views/collectManage/collectManage'),
    meta: {
      name: '收藏',
      noRenderRoute: true
    }
  },
  {
    path: '/messageManage',
    name: 'messageManage',
    component: () => import('@/views/messageManage/messageManage'),
    meta: {
      name: '消息',
      noRenderRoute: true
    }
  },
  {
    path: '/myModel',
    name: 'myModel',
    component: () => import('@/views/myModel/myModel'),
    meta: {
      name: '我的模型',
      noRenderRoute: true
    }
  }
]

const routers = getRouters(routes)
const MODELBASEROUTES = storage.get('MODELBASEROUTES')
if (MODELBASEROUTES) {
  storage.clear('MODELBASEROUTES')
}
storage.set('MODELBASEROUTES', routers)
function getRouters(router) {
  const routersArr = []
  getRoutersByfor(router)
  function getRoutersByfor(router) {
    router.forEach(item => {
      if (item.meta && item.meta.name) {
        let namepath = Object.assign({}, item.meta, { path: item.path })
        routersArr.push(namepath)
      }
      if (item.children && item.children.length > 0) {
        getRoutersByfor(item.children)
      }
    })
  }
  return routersArr
}

export default routes
