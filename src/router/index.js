import VueRouter from 'vue-router'
import routes from './routes.js'
import Vue from 'vue'
Vue.use(VueRouter)
const router = new VueRouter({
  base: window.__POWERED_BY_QIANKUN__ ? '/subApp/' : '/',
  mode: 'hash',
  routes
})
export default router
