import { storage } from '@/utils/global'

const moduleStore = {
  state: {
    messageVal: storage.get('MVAL') || 5, // 消息数量
    userInfo: '',
    routerInfo: storage.get('MODELBASEROUTES') || [],
    color: [
      'rgba(50, 207, 182, 1)',
      'rgba(255, 200, 70, 1)',
      'rgba(101, 183, 254, 1)',
      'rgba(155, 106, 255, 1)',
      'rgba(255, 113, 94, 1)',
      'rgba(84, 112, 198, 1)',
      'rgba(145, 204, 117, 1)',
      'rgba(250, 200, 88, 1)',
      'rgba(238, 102, 102, 1)',
      'rgba(165, 192, 222, 1)',
      'rgba(59, 162, 114, 1)',
      'rgba(250, 132, 82, 1)',
      'rgba(154, 96, 180, 1)',
      'rgba(234, 124, 204, 1)'
    ]
  },
  mutations: {
    // 通知数量
    setMessageVal(state, value) {
      state.messageVal = value
      storage.set('MVAL', value)
    },
    // 用户信息
    setUserInfo(state, value) {
      state.userInfo = value
      storage.set('USERINFO', value)
    },
    //
    setActiveHeader(state, value) {},
    setRouterInfo(state, value) {
      const routers = getRouters(value)
      function getRouters(router) {
        const routersArr = []
        getRoutersByfor(router)
        function getRoutersByfor(router) {
          router.forEach(item => {
            const namepath = { name: '', path: '' }
            if (item.meta && item.meta.title) {
              namepath.name = item.meta.title
              namepath.path = item.path
              routersArr.push(namepath)
            }
            if (item.children && item.children.length > 0) {
              getRoutersByfor(item.children)
            }
          })
        }
        return routersArr
      }
      state.routerInfo = value
      storage.set('MODELBASEROUTES', routers)
    }
  },
  actions: {},
  getters: {}
}
export default moduleStore
