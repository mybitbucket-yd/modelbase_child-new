import Vue from 'vue'
import Vuex from 'vuex'
import moduleStore from './modules/moduleStore'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    moduleStore
  }
})
